package com.flycms.web.admin;

import com.flycms.constant.Const;
import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.core.entity.PageVo;
import com.flycms.module.admin.model.Admin;
import com.flycms.module.admin.service.AdminService;
import com.flycms.module.security.model.Role;
import com.flycms.module.security.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-4
 */
@Controller
@RequestMapping("/admin/user")
public class AdminController extends BaseController {

    @Autowired
    protected AdminService adminService;

    @Autowired
    protected RoleService roleService;

    //用户列表
    @GetMapping(value = "/admin_list")
    public String adminList(@RequestParam(value = "adminName", required = false) String adminName,
                            @RequestParam(value = "nickName", required = false) String nickName,
                            @RequestParam(value = "mobile", required = false) String mobile,
                            @RequestParam(value = "email", required = false) String email,
                            @RequestParam(value = "p", defaultValue = "1") int pageNum,
                            ModelMap modelMap){
        PageVo<Admin> pageVo=adminService.getAdminListPage(adminName, nickName, mobile, email,pageNum,20);
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("role", role);
        modelMap.put("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/admin_list");
    }

    //添加管理员
    @GetMapping(value = "/admin_add")
    public String adminAdd(ModelMap modelMap){
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("role", role);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/admin_add");
    }

    //处理和保存管理员信息
    @PostMapping("/admin_save")
    @ResponseBody
    public DataVo addAdminSave(@Valid Admin admin, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = adminService.addAdmin(admin);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //编辑用户
    @GetMapping(value = "/admin_edit/{adminId}")
    public String adminEdit(@PathVariable(value = "adminId", required = false) int adminId,ModelMap modelMap){
        Admin user=adminService.findAdminById(adminId,0);
        if(user==null){
            return theme.getAdminTemplate("404");
        }
        int roleId=roleService.findAdminAndRoleById(adminId);
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("roleId", roleId);
        modelMap.put("role", role);
        modelMap.addAttribute("admin", getAdminUser());
        modelMap.addAttribute("user", user);
        return theme.getAdminTemplate("webconfig/admin_edit");
    }

    //处理和保存更新后管理员信息
    @PostMapping("/admin_act")
    @ResponseBody
    public DataVo addAdminAct(@Valid Admin admin, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = adminService.updateAdmin(admin);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //删除管理员
    @PostMapping("/delAdmin")
    @ResponseBody
    public DataVo deleteAdminById(@RequestParam(value = "id") int id){
        DataVo data = DataVo.failure("操作失败");
        if(id==1){
            return data = DataVo.failure("超级管理员组不能删除");
        }
        data = adminService.deleteAdminById(id);
        return data;
    }

    //我的密码修改
    @GetMapping(value = "/admin_password")
    public String userPassword(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/admin_password");
    }

    @ResponseBody
    @PostMapping(value = "/password_update")
    public DataVo updatePassword(
            @RequestParam(value = "old_password", required = false) String old_password,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "password_confirmation", required = false) String password_confirmation) {
        String kaptcha = (String) session.getAttribute("kaptcha");
        DataVo data = DataVo.failure("操作失败");
        try {
            if (StringUtils.isBlank(old_password)) {
                return DataVo.failure("原来密码不能为空");
            } else if (old_password.length() < 6 && old_password.length() >= 32) {
                return DataVo.failure("密码最少6个字符，最多32个字符");
            }
            if (StringUtils.isBlank(password)) {
                return DataVo.failure("新密码不能为空");
            } else if (password.length() < 6 && password.length() >= 32) {
                return DataVo.failure("密码最少6个字符，最多32个字符");
            }
            if (!password.equals(password_confirmation)) {
                return DataVo.failure("两次密码必须一样");
            }
            data=adminService.updateAdminPassword(getAdminUser().getId(), old_password, password);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }
}
