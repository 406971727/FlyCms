package com.flycms.web.admin;

import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.module.search.service.SolrService;
import com.flycms.module.share.service.ShareService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 12:05 2018/9/3
 */
@Controller
@RequestMapping("/admin/share")
public class ShareAdminController extends BaseController {
    @Autowired
    private ShareService shareService;

    @Autowired
    private SolrService solrService;

    @GetMapping(value = "/list_share")
    public String shareList(@RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap){
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("content/list_share");
    }

    @ResponseBody
    @RequestMapping(value = "/index_all_share")
    public DataVo indexAllShare() {
        DataVo data = DataVo.failure("操作失败");
        try {
            solrService.indexAllShare();
            data=DataVo.success("全部索引成功！");
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }
}
