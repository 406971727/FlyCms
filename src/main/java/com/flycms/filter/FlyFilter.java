package com.flycms.filter;

import com.flycms.core.utils.CookieUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.annotation.WebFilter;
import javax.servlet.*;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * 全站过滤器，获取用户url携带邀请参数，记录邀请人id
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 9:51 2018/9/12
 */

@WebFilter(filterName="myFilter",urlPatterns="/*")
public class FlyFilter implements Filter {
    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //System.out.println("MyFilter init............");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String invite=request.getParameter("invite");
        if(!StringUtils.isBlank(invite)){
            CookieUtils.writeCookie(httpResponse,"invite",invite,60*60*24*7);
        }
        //System.out.println("MyFilter doFilter.........before");
        chain.doFilter(request, response);
        //System.out.println("MyFilter doFilter.........after");
    }

    @Override
    public void destroy() {
        //System.out.println("MyFilter destroy..........");
    }
}